#ifndef WY_NUMPY_HANDLER_H
#define WY_NUMPY_HANDLER_H

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <pybind11/numpy.h>

#ifdef WYRM_USE_CUDA
#include <cuComplex.h>

#ifdef WYRM_DO_HALF
#include <cuda_fp16.h>
#endif

#endif

namespace wy {

using pybind11::detail::is_pod_struct;

template<typename T>
using enableAdapt = typename std::enable_if<(std::is_scalar<T>::value or is_pod_struct<T>::value) and  not std::is_const<T>::value>::type;

template<typename T>
using array_c = pybind11::array_t<T, pybind11::array::c_style | pybind11::array::forcecast>;

template <typename T, typename = void>
struct RowMajorAdapter
{
  using type = T;

  static T cast(type t) { return t; }
};

template <typename T>
struct RowMajorAdapter<const T *, enableAdapt<T>> {
  using type = const array_c<T> &;

  static const T *cast(type t) { return t.data(); }
};

template <typename T>
struct RowMajorAdapter<T *, enableAdapt<T>> {
  using type = array_c<T> &;

  static T *cast(type t) { return t.mutable_data(); }
};

template<>
struct RowMajorAdapter<const char *>
{
  using type = const char *;

  static type cast(type t) { return t; }
};

template<>
struct RowMajorAdapter<char *>
{
  using type = char *;

  static type cast(type t) { return t; }
};

template<typename T>
using array_f = pybind11::array_t<T, pybind11::array::f_style | pybind11::array::forcecast>;

template <typename T, typename = void>
struct ColMajorAdapter
{
  using type = T;

  static T cast(type t) { return t; }
};

template <typename T>
struct ColMajorAdapter<const T *, enableAdapt<T>> {
  using type = const array_f<T> &;

  static const T *cast(type t) { return t.data(); }
};

template <typename T>
struct ColMajorAdapter<T *, enableAdapt<T>> {
  using type = array_f<T> &;

  static T *cast(type t) { return t.mutable_data(); }
};

template<>
struct ColMajorAdapter<const char *>
{
  using type = const char *;

  static type cast(type t) { return t; }
};

template<>
struct ColMajorAdapter<char *>
{
  using type = char *;

  static type cast(type t) { return t; }
};

}  // namespace wy

#ifdef WYRM_USE_CUDA

using complex64 = cuFloatComplex;
static_assert(sizeof(complex64) == 8, "Bad size");

using complex128 = cuDoubleComplex;
static_assert(sizeof(complex128) == 16, "Bad size");

namespace pybind11 { namespace detail {

// Similar to enums in `pybind11/numpy.h`. Determined by doing:
// python3 -c 'import numpy as np; print(np.dtype(np.float16).num)'
constexpr int NPY_COMPLEX64 = 14;
constexpr int NPY_COMPLEX128 = 15;

// Kinda following: https://github.com/pybind/pybind11/blob/9bb3313162c0b856125e481ceece9d8faa567716/include/pybind11/numpy.h#L1000
template <>
struct npy_format_descriptor<complex64> {
  static pybind11::dtype dtype() {
    handle ptr = npy_api::get().PyArray_DescrFromType_(NPY_COMPLEX64);
    return reinterpret_borrow<pybind11::dtype>(ptr);
  }
  static std::string format() {
    // following: https://docs.python.org/3/library/struct.html#format-characters
    return "Zf";
  }
  static constexpr auto name = _("complex64");
};

// Kinda following: https://github.com/pybind/pybind11/blob/9bb3313162c0b856125e481ceece9d8faa567716/include/pybind11/numpy.h#L1000
template <>
struct npy_format_descriptor<complex128> {
  static pybind11::dtype dtype() {
    handle ptr = npy_api::get().PyArray_DescrFromType_(NPY_COMPLEX128);
    return reinterpret_borrow<pybind11::dtype>(ptr);
  }
  static std::string format() {
    // following: https://docs.python.org/3/library/struct.html#format-characters
    return "Zd";
  }
  static constexpr auto name = _("complex128");
};

template <typename T>
struct npy_scalar_caster {
  PYBIND11_TYPE_CASTER(T, _("PleaseOverride"));
  using Array = wy::array_f<T>;

  bool load(handle src, bool convert) {
    // Taken from Eigen casters. Permits either scalar dtype or scalar array.
    handle type = dtype::of<T>().attr("type");  // Could make more efficient.
    if (!convert && !isinstance<Array>(src) && !isinstance(src, type))
      return false;
    Array tmp = Array::ensure(src);
    if (tmp && tmp.size() == 1 && tmp.ndim() == 0) {
      this->value = *tmp.data();
      return true;
    }
    return false;
  }

  static handle cast(T src, return_value_policy, handle) {
    Array tmp({1});
    tmp.mutable_at(0) = src;
    tmp.resize({});
    // You could also just return the array if you want a scalar array.
    object scalar = tmp[tuple()];
    return scalar.release();
  }
};

template <>
struct type_caster<complex64> : npy_scalar_caster<complex64> {
  static constexpr auto name = _("complex64");
};

template <>
struct type_caster<complex128> : npy_scalar_caster<complex128> {
  static constexpr auto name = _("complex128");
};

#ifdef WYRM_DO_HALF

using float16 = half;
static_assert(sizeof(float16) == 2, "Bad size");

// Similar to enums in `pybind11/numpy.h`. Determined by doing:
// python3 -c 'import numpy as np; print(np.dtype(np.float16).num)'
constexpr int NPY_FLOAT16 = 23;

// Kinda following:
// https://github.com/pybind/pybind11/blob/9bb3313162c0b856125e481ceece9d8faa567716/include/pybind11/numpy.h#L1000
template <>
struct npy_format_descriptor<float16> {
  static pybind11::dtype dtype() {
    handle ptr = npy_api::get().PyArray_DescrFromType_(NPY_FLOAT16);
    return reinterpret_borrow<pybind11::dtype>(ptr);
  }
  static std::string format() {
    // following:
    // https://docs.python.org/3/library/struct.html#format-characters
    return "e";
  }
  static constexpr auto name = _("float16");
};

template <>
struct type_caster<float16> : npy_scalar_caster<float16> {
  static constexpr auto name = _("float16");
};

#endif

}}  // namespace pybind11::detail

#endif

#endif  // WY_NUMPY_HANDLER_H
