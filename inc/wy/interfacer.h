#ifndef WYRM_INTERFACER_H
#define WYRM_INTERFACER_H

namespace wy {

template <typename...>
struct TypeList;

template <typename Type, typename... Types>
struct TypeList<Type, Types...> {
  using Head = Type;
  using Tail = TypeList<Types...>;
};

template <>
struct TypeList<> {};

using EmptyList = TypeList<>;

template <typename Interfacer, typename TList>
struct TypeMap;

template <typename Interfacer>
struct TypeMap<Interfacer, EmptyList> {
  template <typename... Args>
  static void apply(Args&&...) {}
};

template <typename Interfacer, typename Type,
          typename... Types  //,
                             // typename TList = TypeList<Type, Types...>,
                             // typename Head = typename TList::Head, typename
                             // Tail = typename TList::Tail
          >
struct TypeMap<Interfacer, TypeList<Type, Types...>>
    : TypeMap<Interfacer, typename TypeList<Type, Types...>::Tail> {
  using TList = TypeList<Type, Types...>;
  using Head = typename TList::Head;
  using Tail = typename TList::Tail;

  using Base = TypeMap<Interfacer, Tail>;

  template <typename... Args>
  static void apply(Args&&... args) {
    Interfacer::template call<Head>(args...);
    Base::template apply(std::forward<Args>(args)...);
  }
};

template <typename Interfacer, typename TList, typename... Args>
void apply(Args&&... args) {
  TypeMap<Interfacer, TList>::template apply(std::forward<Args>(args)...);
}
}

#endif  // WYRM_INTERFACER_H
