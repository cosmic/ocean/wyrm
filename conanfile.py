from conans import ConanFile, tools, CMake


class WyrmConan(ConanFile):
    name = "wyrm"
    version = "0.4"
    settings = "os", "compiler", "arch", "build_type"
    description = "Add usefull features to pybind11."
    license = "MIT"
    url = "https://gitlab.obspm.fr/cosmic/ocean/wyrm"

    requires = ['pybind11/2.9.1@cosmic/stable']
    generators = "cmake"
    options = {
        'cuda': [True, False],
        'half': [True, False]}
    default_options = {'cuda' : True, 'half' : True}

    exports_sources = ["inc/*", "CMakeLists.txt", "test/*"]
    no_copy_sources = True

    def requirements(self):
        if self.options.half:
            self.requires('half/2.1.0@cosmic/stable')

    def package(self):
        self.copy('*.h', dst='include', src='inc')
        self.copy('wyrm', dst='include', src='inc')

    def package_id(self):
        self.info.header_only()

    def package_info(self):
        if self.options.cuda:
            self.cpp_info.defines += ['WYRM_USE_CUDA=1']
        if self.options.half:
            self.cpp_info.defines += ['WYRM_DO_HALF=1']
